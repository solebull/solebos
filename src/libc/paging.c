#include "paging.h"

#include <stdint.h>

#include "../cpu/isr.h"        // Uses register_t
#include "../drivers/screen.h" // Uses kprint*
#include "../kernel/panic.h"   // Uses PANIC
#include "mem.h"
#include "function.h"          // Uses UNUSED

#define NB_PAGES 400 // Reboot if 1024 :(

uint32_t page_directory[NB_PAGES] __attribute__((aligned(4096)));
uint32_t first_page_table[NB_PAGES] __attribute__((aligned(4096)));

void load_page(uint32_t* dir)
{
#ifndef _TESTS
   asm volatile("mov %0, %%cr3":: "r"(dir));
#else
   UNUSED(dir);
#endif
}

/** Set the cr0 register to 0x80000000 to enable paging
  *
  * see https://littleosbook.github.io/#paging.
  *
  * \return The value affected to cr0.
  *
  */
uint32_t
enable_paging()
{
  kprint_at("[Paging mode]\n", 15, 0);
  kprint_at("\n", 0, 13);
  uint32_t cr0=0;
#ifndef _TESTS
  asm volatile("mov %%cr0, %0": "=r"(cr0));
  cr0 |= 0x80000000; // Enable paging!
  asm volatile("mov %0, %%cr0":: "r"(cr0));
#else
  cr0 |= 0x80000000; // The value affected to cr0 register
#endif // !_TESTS
  return cr0;
}


heap_t*
initialise_paging()
{
  heap_t* heap;
  
  //set each entry to not present
  unsigned int i;
  for(i = 0; i < NB_PAGES; i++)
    {
      // This sets the following flags to the pages:
      //   Supervisor: Only kernel-mode can access them
      //   Write Enabled: It can be both read from and written to
      //   Not Present: The page table is not present
      page_directory[i] = 0x00000002;

      // As the address is page aligned, it will always leave 12 bits zeroed.
      // Those bits are used by the attributes ;)
      first_page_table[i] = (i * 0x1000) | 3; // attributes: supervisor level, read/write, present.

    }
  
  
  // attributes: supervisor level, read/write, present
  page_directory[0] = ((unsigned int)first_page_table) | 3;
  
  load_page(page_directory);


  // Before we enable paging, we must register our page fault handler.
  register_exception_handler(14, page_fault);

  // And create the heap
  heap = create_heap(KHEAP_START, KHEAP_START+KHEAP_INITIAL_SIZE, 0xCFFFF000,
		      0, 0);
  
  enable_paging();
  // Initialise the kernel heap.
  return heap;
}

/** A segfaul occured. Prints info and panic
  *
  * \param regs The current registers.
  *
  */
void
page_fault(registers_t* regs)
{
   // A page fault has occurred.
   // The faulting address is stored in the CR2 register.
   uint32_t faulting_address;
#ifndef _TESTS
   asm volatile("mov %%cr2, %0" : "=r" (faulting_address));
#else
   faulting_address=0x1000; // Example adress to avoid 'unused variable' warning
#endif //!_TESTS
   
   // The error code gives us details of what happened.
   int present   = !(regs->err_code & 0x1); // Page not present
   int rw = regs->err_code & 0x2;           // Write operation?
   int us = regs->err_code & 0x4;           // Processor was in user-mode?
   int reserved = regs->err_code & 0x8;     // Overwritten CPU-reserved bits of page entry?
   int id = regs->err_code & 0x10;          // Caused by an instruction fetch?

   // Output an error message.
   kprint("Page fault! ( ");
   if (present) {kprint("present ");}
   if (rw) {kprint("read-only ");}
   if (us) {kprint("user-mode ");}
   if (reserved) {kprint("reserved ");}
   kprint(") at ");
   kprint_hex(faulting_address);
   kprint("\n");
   PANIC("Page fault");
   UNUSED(id);
}

uint32_t
get_page(uint32_t addr, bool create, dir_t dir)
{
  // Turn the address into an index.
   addr /= 0x1000;
   // Find the page table containing this address.
   uint32_t table_idx = addr / 1024;
   UNUSED(dir);
   UNUSED(create);
   load_page(&first_page_table[table_idx]);
   return first_page_table[table_idx];
   /*   if (dir->tables[table_idx]) // If this table is already assigned
   {
       return &dir->tables[table_idx]->pages[address%1024];
   }
   else if(make)
   {
       u32int tmp;
       dir->tables[table_idx] = (page_table_t*)kmalloc_ap(sizeof(page_table_t), &tmp);
       memset(dir->tables[table_idx], 0, 0x1000);
       dir->tablesPhysical[table_idx] = tmp | 0x7; // PRESENT, RW, US.
       return &dir->tables[table_idx]->pages[address%1024];
   }
   else
   {
       return 0;
   }
   */
}
