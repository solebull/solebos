#ifndef _BOOL_H_
#define _BOOL_H_

/** A very simple bool type */
typedef enum {
  false = 0,
  true  = !false
} bool;

#define BOOL_DEFINED

#endif // !_BOOL_H_
