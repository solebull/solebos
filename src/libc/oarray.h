#ifndef _OARRAY_H_
#define _OARRAY_H_

/** \file oarray.h
  *
  * A insertion_time sorted array
  * see http://www.jamesmolloy.co.uk/tutorial_html/7.-The%20Heap.html
  *
  * Mainly used to implement heap in heap.h.
  *
  */

#include <stdint.h>

/**
  * This array is insertion sorted - it always remains in a sorted state 
  * (between calls). It can store anything that can be cast to a void* -- 
  * so a uint32_t, or any pointer.
  *
  */
typedef void* type_t;
/**
  * A predicate should return nonzero if the first argument is less than 
  * the second. Else it should return zero.
  *
  */
typedef int8_t (*lessthan_predicate_t)(type_t,type_t);


/** The Ordered array type 
  *
  * Actually mainly used to implement head.
  *
  */
typedef struct
{
  type_t*  array;                 //!< The array
  uint32_t size;                  //!< The size in number of elements
  uint32_t max_size;              //!< The max allocated size
  lessthan_predicate_t less_than; //!< The predicate used to order elements
} ordered_array_t;

/**
  A standard less than predicate.
**/
int8_t standard_lessthan_predicate(type_t a, type_t b);

ordered_array_t* create_ordered_array(uint32_t, lessthan_predicate_t);
ordered_array_t place_ordered_array(void*, uint32_t, lessthan_predicate_t);

/**
  Destroy an ordered array.
**/
void destroy_ordered_array(ordered_array_t *array);

/**
  Add an item into the array.
**/
void insert_ordered_array(type_t item, ordered_array_t *array);

/**
  Lookup the item at index i.
**/
type_t lookup_ordered_array(uint32_t i, ordered_array_t *array);

/**
  Deletes the item at location i from the array.
**/
void remove_ordered_array(uint32_t i, ordered_array_t *array);


#endif // !_OARRAY_H_
