#ifndef _HISTORY_H_
#define _HISTORY_H_

#define MAX_HISTORY_SIZE 40

/** /file history.h
  *    
  * Handle the command history.
  *
  */

typedef struct
{
  char* array[MAX_HISTORY_SIZE] ;
  
} history_t;

history_t* history_create();

#endif // !_HISTORY_H_
  
