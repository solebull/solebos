#include "paging_status.h"

#include "libc/function.h"   // for UNUSED
#include "libc/mem.h"        // for kmalloc_a

/** Create a new paging_status_t struct and set default values
  *
  */
paging_status_t*
create_paging_status(void)
{
  paging_status_t* to_ret;
  to_ret = (paging_status_t*)kmalloc_a(sizeof(paging_status_t));
  to_ret->enabled = false;
  to_ret->cr0_set = false;
  return to_ret;
		
}

/** Destroy the memory allocated for the given structure
  *
  * \param ps a Dynamically allocated paging_status_t structure.
  *
  */
void
destroy_paging_status(paging_status_t* ps)
{
  UNUSED(ps);
  //  kfree(ps);
}

