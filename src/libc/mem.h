#ifndef MEM_H
#define MEM_H

#include <stdint.h>
#include <stddef.h>

void memcpy(uint8_t *source, uint8_t *dest, int nbytes);
void memset(uint8_t *dest, uint8_t val, uint32_t len);

/* At this stage there is no 'free' implemented. */
void* kmalloc(size_t size, int align, uint32_t *phys_addr);
void* kmalloc_ap(size_t size, uint32_t *phys_addr);
void* kmalloc_sz(uint32_t size);
void* kmalloc_a(uint32_t sz);

#endif // !MEM_H
