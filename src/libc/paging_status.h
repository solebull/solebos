#ifndef __PAGING_STATUS_H__
#define __PAGING_STATUS_H__

#include "bool.h"        // Uses bool

/** A struct used to check and control paging enabling status
  *
  * See https://littleosbook.github.io/#paging-in-x86 on how to enable it.
  *
  */
typedef struct
{
  // Global status
  bool enabled; //!< Is the paging enabled. Set at the end

  // Step-based status
  bool cr0_set; //!< Was the cr0 bit correctly set to 1 ?
}paging_status_t;

paging_status_t* create_paging_status(void);
void             destroy_paging_status(paging_status_t*);

#endif // !__PAGING_STATUS_H__
