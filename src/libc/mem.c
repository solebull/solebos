#include "mem.h"

#include "kernel/panic.h"
#include "kernel/heap.h"
#include "libc/paging.h" // For dir_t
#include "libc/function.h" // For UNUSED

#ifdef _TESTS
#  include <stdlib.h>
#endif

extern heap_t *kheap; // from kernel/kernel.c

/* This should be computed at link time, but a hardcoded
 * value is fine for now. Remember that our kernel starts
 * at 0x1000 as defined on the Makefile */
uint32_t free_mem_addr = 0x10000;

void memcpy(uint8_t *source, uint8_t *dest, int nbytes) {
  int i;
  for (i = 0; i < nbytes; i++) {
    *(dest + i) = *(source + i);
  }
}

void memset(uint8_t *dest, uint8_t val, uint32_t len) {
#ifdef _TESTS
  UNUSED(dest);
  UNUSED(val);
  UNUSED(len);
#else  
  uint8_t *temp = (uint8_t *)dest;
  for ( ; len != 0; len--)
    *temp++ = val;
#endif
}

/* Implementation is just a pointer to some free memory which
 * keeps growing */
void* kmalloc(size_t size, int align, uint32_t *phys_addr) {
#ifdef _TESTS
  return malloc(size);
#endif
  if (kheap != NULL)
    {
      uint32_t addr = (uint32_t)halloc(size, (uint8_t)align, kheap);
        if (phys_addr != 0)
        {
            uint32_t page = get_page(addr, 0, KERNEL_DIRECTORY);
            *phys_addr = page;
        }
        return (void*)addr;
    }
    else
    {
      /* Pages are aligned to 4K, or 0x1000 */
      if (align == 1 && (free_mem_addr & 0xFFFFF000)) {
	free_mem_addr &= 0xFFFFF000;
	free_mem_addr += 0x1000;
      }
      /* Save also the physical address */
      if (phys_addr) *phys_addr = free_mem_addr;
      
      uint32_t ret = free_mem_addr;
      free_mem_addr += size; // Remember to increment the pointer
      return (void*)ret;
    }
}

void*
kmalloc_ap(size_t size, uint32_t *phys_addr)
{
  return kmalloc(size, 1, phys_addr);
}

void* kmalloc_sz(uint32_t size)
{
    return kmalloc(size, 0, 0);
}

void* kmalloc_a(uint32_t sz)
{
  return kmalloc(sz, 0, 0);
}
