#ifndef _PAGING_H_
#define _PAGING_H_

#include "kernel/heap.h" // Uses heap_t
#include "cpu/isr.h"     // Uses registers_t
#include "bool.h"        // Uses bool

typedef enum {
  KERNEL_DIRECTORY
}dir_t;

heap_t* initialise_paging(void);
void    page_fault(registers_t*);
uint32_t get_page(uint32_t, bool, dir_t);


/// Internal. For unit tests purpose only
uint32_t enable_paging(void);


#endif  // !_PAGING_H_
