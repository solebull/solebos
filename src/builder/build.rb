#Build type handling
class Build
  @type='debug'  # The global build type
  
  # Cerate the object using the ARGV array as command line argument
  def initialize(argv)
    if argv.include? "help"
      print_help
      exit(0)
    elsif argv.include? "test"
      @build="test"
    elsif argv.include? "release"
      @build="release"
    else
      puts "No action found. Assuming 'debug' release type."
    end

    argv.each do |a|
      puts a
    end
  end
  
  def print_help
    
    puts <<EOF
./buil   der - solebOS builder script and Makefile generator.
         
./builder        [help||test||release] 
  Shows this hel p text or defines a build type (default to 'debug').
  
EOF
  
  end
  
end
