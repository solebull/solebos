#ifndef ISR_H
#define ISR_H

#include <stdint.h>

/* ISRs reserved for CPU exceptions */
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
/* IRQ definitions */
extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();
// see https://www.webopedia.com/quick_ref/IRQnumbers.asp
#define IRQ0 32  //!< System timer
#define IRQ1 33  //!< Keyboard
#define IRQ2 34  //!< Cascade interrupt for IRQs 8-15
#define IRQ3 35  //!< Second serial port (COM2)
#define IRQ4 36  //!< First serial port (COM1)
#define IRQ5 37  //!< Sound card
#define IRQ6 38  //!< Floppy disk controller
#define IRQ7 39  //!< First parallel
#define IRQ8 40  //!< Real-time clock
#define IRQ9 41  //!< Open interrupt
#define IRQ10 42 //!< Open interrupt
#define IRQ11 43 //!< Open interrupt
#define IRQ12 44 //!< PS/2 mouse
#define IRQ13 45 //!< Floating-point unit
#define IRQ14 46 //!< Primary IDE channel
#define IRQ15 47 //!< Secondary IDE channel


/** Struct which aggregates many registers.
  * It matches exactly the pushes on interrupt.asm. From the bottom:
  * - Pushed by the processor automatically
  * - `push byte`s on the isr-specific code: error code, then int number
  * - All the registers by pusha
  * - `push eax` whose lower 16-bits contain DS
  *
  *  edi, esi, ebp, useless, ebx, edx, ecx and eax apr pushed by pusha.
  *  eip, cs, eflags, esp and ss are pushed by the processor automatically.
  *
  */
typedef struct {
  uint32_t ds; //!< Data segment selector
   uint32_t edi, esi, ebp, useless, ebx, edx, ecx, eax; /* Pushed by pusha. */
   uint32_t int_no, err_code; /* Interrupt number and error code (if applicable) */
  /** Holds the Extended Instruction Pointer
    *
    * It tells the computer where to go next to execute the next command and 
    * controls the flow of a program.
    *
    */
  uint32_t eip;
  /** Points to the top of the stack at any time
    *
    * Serves as an indirect memory operand pointing to the top of the stack 
    * at any time.
    * see http://www.c-jump.com/CIS77/ASM/Stack/S77_0040_esp_register.htm
    *
    */
  uint32_t esp;
  uint32_t cs, eflags;
  /**  Holds the Stack segment your program uses. 
    *
    * Sometimes has the same value as DS.
    * Changing its value can give unpredictable results,
    * mostly data related.
    *
    */
  uint32_t ss;
} registers_t;

/** The interrupt handler type */
typedef void (*isr_t)(registers_t*);

void isr_install();
void isr_handler(registers_t *r);
void irq_install();

void register_interrupt_handler(uint8_t n, isr_t handler);
void register_exception_handler(uint8_t n, isr_t handler);

#endif // !ISR_H
