; Load the given page directory

	global loadPageDirectory
	global enablePaging

loadPageDirectory:
	push ebp
	mov esp, ebp
	mov 8(%esp), %eax
	mov %eax, %cr3
	mov %ebp, %esp
	pop %ebp

	ret
	
	

; Set the 32th bit oc CR0 register to enable paging
enablePaging:
	push %ebp
	mov %esp, %ebp
	mov %cr0, %eax
	or $0x80000000, %eax
	mov %eax, %cr0
	mov %ebp, %esp
	pop %ebp
	ret
