#include "idt.h"
#include "type.h"

idt_gate_t idt[IDT_ENTRIES]; //!< The idt_gate_t array
idt_register_t idt_reg;      //!< Array of interrupt handlers

/** Set given handle to the given idt array index
  *
  * \param n       The index in the idt array. Shouldn't exceed IDT_ENTRIES.
  * \param handler The high_offset
  *
  */
void set_idt_gate(int n, uint32_t handler) {
    idt[n].low_offset = low_16(handler);
    idt[n].sel = KERNEL_CS;
    idt[n].always0 = 0;
    idt[n].flags = 0x8E; 
    idt[n].high_offset = high_16(handler);
}

/** Set the idt_reg values and make the asm lidtl() function use them
  *
  */
void set_idt() {
    idt_reg.base = (uint32_t) &idt;
    idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;
#ifndef _TESTS
    /* Don't make the mistake of loading &idt -- always load &idt_reg */
    asm volatile("lidtl (%0)" : : "r" (&idt_reg));
#endif //!_TESTS
}
