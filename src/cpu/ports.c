#include "ports.h"

/**
 * Read a byte from the specified port
 *
 * \param port The port to get data from.
 * 
 * \return The result value  or 0 if in unit tests mode.
 *
 */
uint8_t
port_byte_in (uint16_t port) {
    uint8_t result=0;
    /* Inline assembler syntax
     * Notice how the source and destination registers are switched from NASM
     *
     * '"=a" (result)'; set '=' the C variable '(result)' to the value of 
     *       register e'a'x
     * '"d" (port)': map the C variable '(port)' into e'd'x register
     *
     * Inputs and outputs are separated by colons
     */
#ifndef _TESTS
    asm("in %%dx, %%al" : "=a" (result) : "d" (port));
#endif
    return result;
}

/** Copy the data of byte length to the given port
  *
  * \param data The data to be copied
  * \param port The port to copy data to
  *
  */
void
port_byte_out (uint16_t port, uint8_t data) {
    /* Notice how here both registers are mapped to C variables and
     * nothing is returned, thus, no equals '=' in the asm syntax 
     * However we see a comma since there are two variables in the input area
     * and none in the 'return' area
     */
#ifndef _TESTS
  asm volatile("out %%al, %%dx" : : "a" (data), "d" (port));
#endif  
}

/**
 * Read a byte from the specified port
 *
 * \param port The port to get data from.
 * 
 * \return The result value or 0 if in unit tests mode.
 *
 */
uint16_t
port_word_in (uint16_t port) {
    uint16_t result=0;
#ifndef _TESTS
    asm("in %%dx, %%ax" : "=a" (result) : "d" (port));
#endif
    return result;
}

/** Copy the data of word length to the given port
  *
  * \param data The data to be copied
  * \param port The port to copy data to
  *
  */
void
port_word_out (uint16_t port, uint16_t data) {
#ifndef _TESTS
    asm volatile("out %%ax, %%dx" : : "a" (data), "d" (port));
#endif
}
