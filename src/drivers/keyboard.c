#include "keyboard.h"
#include "../cpu/ports.h"
#include "../cpu/isr.h"
#include "screen.h"
#include "../libc/string.h"
#include "../libc/function.h"
#include "../libc/bool.h"       // Uses bool type
#include "../kernel/kernel.h"
#include <stdint.h>

// For scan codes see https://wiki.osdev.org/PS/2_Keyboard#Scan_Code_Set_1
#define BACKSPACE       0x0E
#define ENTER           0x1C
#define KEY_CAPSLOCK    0x3A 

#define KEY_NUMLOCK     0xC5

#define KEYPAD_MIN      0xC7 // Minimum keypad scancode value
#define KEYPAD_MAX      0xD3 // Maximum keypad scancode value

// For history handling
#define KEY_UP    0x48
#define KEY_DOWN  0x50

#define KEY_LEFT_SHIFT_PRESSED   0x2A
#define KEY_LEFT_SHIFT_RELEASED  0xAA
#define KEY_RIGHT_SHIFT_PRESSED   0x36
#define KEY_RIGHT_SHIFT_RELEASED 0xB6

#define SC_MAX 58            // The max scancode we handle

static char key_buffer[256];

/** Is the capslock key in use */
bool capslock  =false;
bool shift_down=false; // Is this key down ?
bool numlock   =true;  // Is numlock down ?

// English keyboard table
const char *sc_name_en[] = { "ERROR", "Esc", "1", "2", "3", "4", "5", "6", 
    "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E", 
        "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl", 
        "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`", 
        "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".", 
        "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char sc_ascii_en[] = { '?', '?', '1', '2', '3', '4', '5', '6',     
    '7', '8', '9', '0', '-', '=', '?', '?', 'Q', 'W', 'E', 'R', 'T', 'Y', 
        'U', 'I', 'O', 'P', '[', ']', '?', '?', 'A', 'S', 'D', 'F', 'G', 
        'H', 'J', 'K', 'L', ';', '\'', '`', '?', '\\', 'Z', 'X', 'C', 'V', 
        'B', 'N', 'M', ',', '.', '/', '?', '?', '?', ' '};

// Frensh keyboard table
const char *sc_name_fr[] = { "ERROR", "Esc", "1", "2", "3", "4", "5", "6", 
    "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "A", "Z", "E", 
        "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl", 
        "Q", "S", "D", "F", "G", "H", "J", "K", "L", "M", "'", "`", 
        "LShift", "\\", "W", "X", "C", "V", "B", "N", "M", ",", ".", 
        "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
// The lower-case version
const char sc_ascii_fr_lower[] = { '?', '?', '&', '?', '"', '\'', '(', '-',     
    '?', '_', '?', '?', ')', '=', '?', '?', 'a', 'z', 'e', 'r', 't', 'y', 
        'u', 'i', 'o', 'p', '^', '$', '?', '?', 'q', 's', 'd', 'f', 'g', 
        'h', 'j', 'k', 'l', 'm', '?', '*', '?', '\\', 'w', 'x', 'c', 'v', 
        'b', 'n', ',', ';', ':', '/', '?', '?', '?', ' '};
// The Upper case version
const char sc_ascii_fr[] = { '?', '?', '1', '2', '3', '4', '5', '6',     
    '7', '8', '9', '0', '-', '=', '?', '?', 'A', 'Z', 'E', 'R', 'T', 'Y', 
        'U', 'I', 'O', 'P', '[', ']', '?', '?', 'Q', 'S', 'D', 'F', 'G', 
        'H', 'J', 'K', 'L', 'M', '?', '`', '?', '\\', 'W', 'X', 'C', 'V', 
        'B', 'N', '.', '/', '?', '?', '?', '?', '?', ' '};

// Starting at 0xC7
const char sc_keypad[] = {'7',
			  '8', '9', '-', '4',
			  '5', '6', '+', '1',
			  '2', '3', '0', '.'};

/** Return false if we don't have to print the given char
  *
  */
bool
should_print(uint8_t scancode)
{
  if (scancode > SC_MAX)
    return false;
  if (scancode == KEY_LEFT_SHIFT_PRESSED ||
      scancode == KEY_LEFT_SHIFT_RELEASED)
    return false;
  if (scancode == KEY_RIGHT_SHIFT_PRESSED ||
      scancode == KEY_RIGHT_SHIFT_RELEASED)
    return false;
  
  return true;
}

bool
is_upper_case()
{
  if (capslock)
    return true;

  if (shift_down)
    return true;

  return false;
}

/** A special handler for keypad keys
  *
  * \return true if the key was handled
  *
  */
bool
handle_keypad(uint8_t scancode)
{
  if (scancode >= KEYPAD_MIN && scancode <= KEYPAD_MAX) {
    if (numlock) {
      char letter;
      int sc = (int)scancode;
      letter = sc_keypad[sc - KEYPAD_MIN];
      char str[2] = {letter, '\0'};
      append(key_buffer, letter);
      kprint(str);
    }
    return true;
  }
  
  return false;
}

static void keyboard_callback(registers_t *regs) {
    /* The PIC leaves us the scancode in port 0x60 */
    uint8_t scancode = port_byte_in(0x60);

    // Special case for numpad * 
    if (scancode == 0xB7 || scancode == 0x37) {

      // Key pressed, early exit
      if (scancode == 0x37) return;
      
      char str[2] = {'*', '\0'};
      append(key_buffer, '*');
      kprint(str);
      return;
    }
    
    if (scancode == KEY_LEFT_SHIFT_PRESSED ||
	scancode == KEY_RIGHT_SHIFT_PRESSED) {
    
      shift_down=true;
      //      kprint("Left key pressed\n");
    }
    else if (KEY_LEFT_SHIFT_RELEASED) {
      //   kprint("Left key released\n");
      /* BUG: it seems the shift keys are release automatically before
	 the actual letter key is pressed.
       */
    }

    if (scancode == KEY_NUMLOCK) {
      numlock = !numlock;
    }

    if (scancode == KEY_UP) {
      kprint("UP");
    }
    if (scancode == KEY_DOWN) {
      kprint("DOWN");
    }
      
    if (handle_keypad(scancode)) return;
    
    // Early exits if char shouldn't be printed
    if (!should_print(scancode)) return;
    
    if (scancode == BACKSPACE) {
        backspace(key_buffer);
        kprint_backspace();
    } else if (scancode == ENTER) {
        kprint("\n");
        user_input(key_buffer); // kernel-controlled function
        key_buffer[0] = '\0';
    } else if (scancode == KEY_CAPSLOCK) {
      capslock = !capslock;
    } else {

      char letter;
      if (is_upper_case()) {
	letter = sc_ascii_fr[(int)scancode];
	shift_down=false; // automatically release shift
      } else {
	letter = sc_ascii_fr_lower[(int)scancode];
      }
      // Remember that kprint only accepts char[]
      char str[2] = {letter, '\0'};
      append(key_buffer, letter);
      kprint(str);
    }
    UNUSED(regs);
}

void init_keyboard() {
   register_interrupt_handler(IRQ1, keyboard_callback); 
}
