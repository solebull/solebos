#include <check.h>

#include "libc_history.h"

#include "libc/history.h"
//#include "libc/function.h" // For UNUSED

START_TEST(test_history_create)
{
  history_t* h=history_create();
  ck_assert_ptr_nonnull(h);

}
END_TEST

Suite*
history_suite(void)
{
    Suite* s = suite_create("libc/history");
    TCase* tc_core = tcase_create("Libc");
    
    tcase_add_test(tc_core, test_history_create);

    suite_add_tcase(s, tc_core);

    return s;
}
