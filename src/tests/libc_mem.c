#include "libc_mem.h"

#include "libc/mem.h"

START_TEST(test_memset)
{
  uint8_t i = 9;
  memset(&i, 0, 1);
  
  //  ck_assert_int_eq(i, 0); // Can't be tested here
  
}
END_TEST

Suite*
mem_suite(void)
{
    Suite* s = suite_create("libc/mem");
    TCase* tc_core = tcase_create("Libc");
    
    tcase_add_test(tc_core, test_memset);

    suite_add_tcase(s, tc_core);

    return s;
}
