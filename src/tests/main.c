#include "libc_oarray.h"
#include "check.h"
#include "cpu_ports.h"
#include "driver_screen.h"
#include "libc_history.h"
#include "libc_paging.h"
#include "libc_paging_status.h"
#include "libc_mem.h"
#include "kernel_heap.h"
#include "kernel_panic.h"
#include "kernel_tty.h"

#ifdef _TEST
#  pragma message "Building using _TEST"
#endif


int
main(void)
{

  int number_failed;
  SRunner *sr;
  
  Suite* s = suite_create("empty suite");
  sr = srunner_create(s);

  srunner_add_suite (sr, screen_suite());
  srunner_add_suite (sr, mem_suite());
  srunner_add_suite (sr, paging_suite());
  srunner_add_suite (sr, tty_suite());
  srunner_add_suite (sr, ports_suite());
  srunner_add_suite (sr, paging_status_suite());
  //  srunner_add_suite (sr, oarray_suite()); // segfault

  srunner_add_suite (sr, history_suite()); // segfault

  /*  kernel */
  //srunner_add_suite (sr, heap_suite());
  //srunner_add_suite (sr, panic_suite());
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  
  //srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
