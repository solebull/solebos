#ifndef _LIBC_PAGING_TESTS_H_
#define _LIBC_PAGING_TESTS_H_

#include "check.h"

Suite* paging_suite(void);

#endif // !_LIBC_PAGING_TESTS_H_
