#ifndef _LIBC_HISTORY_TESTS_H_
#define _LIBC_HISTORY_TESTS_H_

#include "check.h"

Suite* history_suite(void);

#endif // !_LIBC_HISTORY_TESTS_H_
