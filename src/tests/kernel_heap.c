#include "kernel_heap.h"

#include "kernel/heap.h"

START_TEST(test_heap_create)
{
  heap_t* h=create_heap(KHEAP_START, KHEAP_START+KHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0);
  ck_assert_ptr_ne(h, NULL);

}
END_TEST


Suite* heap_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("kernel/heap");

    /* Core test case */
    tc_core = tcase_create("kernel/heap");

    tcase_add_test(tc_core, test_heap_create);
    suite_add_tcase(s, tc_core);

    return s;

}

