#include "libc_paging_status.h"

#include "libc/paging_status.h"
#include "libc/function.h" // For UNUSED

START_TEST(test_paging_status_ctor)
{

}
END_TEST

Suite*
paging_status_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("libc/paging_status");

    /* Core test case */
    tc_core = tcase_create("Libc");

    tcase_add_test(tc_core, test_paging_status_ctor);
    suite_add_tcase(s, tc_core);

    return s;
}
