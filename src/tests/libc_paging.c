#include "libc_paging.h"

#include "../src/libc/paging.h"

#include <stdio.h>  // Uses printf()

/** Test the adress computation function
  *
  */
START_TEST(test_paging_init)
{
  initialise_paging();
  // Simply print paged adress but print is deactivated by default.
  
  uint32_t mem = 0x10000;

  int i;
  for (i=0; i<50; i++)
    {
      //     printf("%#10x\n", mem);
      mem &= 0xFFFFF000;
      mem += 0x1000;
    }
  ck_assert_int_eq(mem, 0x42000);
}
END_TEST

/** Check the value affected to cr0 is correct */
START_TEST(test_enable_paging)
{
  uint32_t cr0=enable_paging();
  ck_assert_int_eq(cr0, 0x80000000);
  printf("Should be %x\n", 0x80000000);
  printf("Actually  %x\n", cr0);
}

/** Create and return a paging-oriented test suite 
  *
  */
Suite *
paging_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("libc/paging");
    
    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_paging_init);
    tcase_add_test(tc_core, test_enable_paging);
    suite_add_tcase(s, tc_core);

    return s;
}
