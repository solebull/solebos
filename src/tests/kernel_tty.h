#ifndef _KERNEL_TTY_TESTS_H_
#define _KERNEL_TTY_TESTS_H_

#include <check.h>

Suite* tty_suite(void);

#endif // !_KERNEL_TTY_TESTS_H_
