#ifndef _LIBC_MEM_TESTS_H_
#define _LIBC_MEM_TESTS_H_

#include "check.h"

Suite* mem_suite(void);

#endif // !_LIBC_MEM_TESTS_H_
