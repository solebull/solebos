#include "driver_screen.h"

#include "drivers/screen.h"
//#include "libc/function.h" // For UNUSED

#include <stdio.h>

START_TEST(test_print_char)
{
  //  ASSERT(NULL); // Just to test assertion macro works
  int offset = print_char('c', 10, 10, 'c');
  ck_assert_int_eq(offset, 1622);
  //  printf("offset is %d\n", offset);
}
END_TEST

/** Return the test suite used for driver/screen functions
  *
  */
Suite* screen_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("driver/screen");

    /* Core test case */
    tc_core = tcase_create("driver/screen");

    tcase_add_test(tc_core, test_print_char);
    suite_add_tcase(s, tc_core);

    return s;
}

