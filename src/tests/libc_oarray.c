#include "libc_oarray.h"

#include "libc/oarray.h"
#include "libc/function.h" // For UNUSED

START_TEST(test_oarray_predicate)
{
  //initialise_paging();
  uint8_t a=0;
  uint8_t b=0;
  int8_t i = standard_lessthan_predicate(&a, &b);
  //  ck_assert_int_eq(i, ?); // 1 for local, 0 for CI
  UNUSED(a);
  UNUSED(b);
  UNUSED(i);
}
END_TEST

START_TEST(test_oarray_create)
{
  //initialise_paging();
  ordered_array_t* oa = create_ordered_array(sizeof(ordered_array_t)*30,
					     &standard_lessthan_predicate);
  UNUSED(oa);
  ck_assert_ptr_ne(oa->array, NULL);
}
END_TEST

START_TEST(test_oarray_destroy)
{
  //initialise_paging();
  ordered_array_t* oa = create_ordered_array(sizeof(ordered_array_t)*30,
					    &standard_lessthan_predicate);
  // Just test for no segfault
  destroy_ordered_array(oa);
  UNUSED(oa);
}
END_TEST


Suite*
oarray_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("libc/oarray");

    /* Core test case */
    tc_core = tcase_create("Libc");

    tcase_add_test(tc_core, test_oarray_predicate);
    tcase_add_test(tc_core, test_oarray_create);
    tcase_add_test(tc_core, test_oarray_destroy);
    suite_add_tcase(s, tc_core);

    return s;
}
