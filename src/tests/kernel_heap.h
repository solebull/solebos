#ifndef _KERNEL_HEAP_TESTS_H_
#define _KERNEL_HEAP_TESTS_H_

#include <check.h>

Suite* heap_suite(void);

#endif // !_KERNEL_HEAP_TESTS_H_
