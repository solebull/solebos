#ifndef _LIBC_PAGING_STATUS_TESTS_H_
#define _LIBC_PAGING_STATUS_TESTS_H_

#include <check.h>

Suite* paging_status_suite(void);

#endif // !_LIBC_PAGING_STATUS_TESTS_H_
