#ifndef _KERNEL_PANIC_TESTS_H_
#define _KERNEL_PANIC_TESTS_H_

#include "check.h"

Suite* panic_suite(void);

#endif // !_KERNEL_PANIC_TESTS_H_
