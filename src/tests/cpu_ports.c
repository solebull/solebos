#include "cpu_ports.h"

#include "cpu/ports.h"

START_TEST(test_port_byte_in)
{
  
  ck_assert_int_eq(port_byte_in(0), 0);
}
END_TEST

START_TEST(test_port_word_in)
{
  ck_assert_int_eq(port_word_in(0), 0);
}
END_TEST

/** Return the test suite used to test cpu/ports.c exported functions
  *
  */
Suite*
ports_suite(void)
{
    Suite* s = suite_create("cpu/ports");
    TCase* tc_core = tcase_create("ports");
    
    tcase_add_test(tc_core, test_port_byte_in);
    tcase_add_test(tc_core, test_port_word_in);

    suite_add_tcase(s, tc_core);

    return s;
}
