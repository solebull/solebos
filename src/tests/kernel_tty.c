#include "kernel_tty.h"

#include "kernel/tty.h"
#include "libc/function.h" // For UNUSED

START_TEST(test_tty_ctor)
{
  //  ASSERT(NULL); // Just to test assertion macro works
  command_t* tty;
  //  tty = initialize_tty(); // Seems to cause CI segfault
  UNUSED(tty);
}
END_TEST


Suite* tty_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("kernel/tty");

    /* Core test case */
    tc_core = tcase_create("kernel/tty");

    tcase_add_test(tc_core, test_tty_ctor);
    suite_add_tcase(s, tc_core);

    return s;
}

