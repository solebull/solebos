#ifndef _LIBC_OARRAY_TESTS_H_
#define _LIBC_OARRAY_TESTS_H_

#include <check.h>

Suite* oarray_suite(void);

#endif // !_LIBC_OARRAY_TESTS_H_
