#include "kernel_panic.h"

#include "kernel/panic.h"

START_TEST(test_panic_assert)
{
  //  ASSERT(NULL); // Just to test assertion macro works
}
END_TEST


Suite* panic_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("kernel/panic");

    /* Core test case */
    tc_core = tcase_create("kernel/panic");
    tcase_add_test(tc_core, test_panic_assert);

    suite_add_tcase(s, tc_core);

    return s;
}

