#include "tty.h"

#include "libc/string.h"    // Uses strlen, strcpy
#include <stddef.h>         // Uses NULL
#include "drivers/screen.h" // Uses kprint*
#include "kernel/panic.h"   // Uses ASSERT
#include "libc/function.h"  // Uses UNUSED
#include "libc/mem.h"       // Uses kmalloc

command_t*
initialize_tty()
{
  command_t* ret =   (command_t*)kmalloc_a(sizeof(command_t));
  ret->next = NULL;
  return ret;
}

void
add_command(command_t* cmd, char* name, const char* help,
	    command_ptr_t ptr)
{
  // Find last actual command
  command_t* now = cmd;
  while (now->next != NULL)
    now = now->next;
  
  // Feed the command
  now->command = (char*)kmalloc_sz(sizeof(char) * (strlen((char*)name) + 1));
  now->command = strcpy(now->command, strupr(name));
  now->ptr = ptr;
  now->next = initialize_tty();
  UNUSED(name);
  UNUSED(help);
}

/** Simply print every registered commands
  *
  */
void
tty_help(command_t* cmd)
{
  kprint("Listing known commands : ");
  command_t* now = cmd;
  while (now->next != NULL)
    {
      kprint(now->command);

      
      now = now->next;
      if (now->next)
	kprint(", ");
    }
  
  kprint(".\n");
}

bool
check_command(command_t* cmds, char* input)
{
  command_t* now = cmds;
  while (now->next != NULL)
    {
      if (strcmp(now->command, strupr(input)) == 0)
	{
	  now->ptr();
	  return true;
	}
      now = now->next;
    }
  return false;
}
