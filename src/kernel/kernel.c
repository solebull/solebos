#include "cpu/isr.h"
#include "drivers/screen.h"
#include "libc/string.h"
#include "libc/mem.h"
#include "libc/paging.h"
#include "libc/function.h" // Uses UNUSED

#include "heap.h" // for halloc
#include "kernel/panic.h" // Uses ASSERT
#include "tty.h"

#include "kernel.h"

#include <stdint.h>

heap_t *kheap = NULL; // Initialized in libc/paging.c
command_t* cmds;

// Forward declarations
static void heap_command();
static void page_command();
static void fault_test();
static void kmalloc_command();
// End of forward declarations

static void help()
{
      tty_help(cmds);
}

void
kernel_main() {
    isr_install();
    irq_install();

    //    asm("int $2");
    //    asm("int $3");

    kheap = initialise_paging();
    //    ASSERT(kheap);
    kprint("Type HELP to see command list\n"
	"> ");
    cmds = initialize_tty();
    // Please try to keep this list sorted
    add_command(cmds, "end",  "Halt the CPU.", &halt);
    add_command(cmds, "fault", "Simulate a segmentation fault.", &fault_test);
    add_command(cmds, "heap", "Test the heap implementation", &heap_command);
    add_command(cmds, "help", "Print all commands", &help);
    add_command(cmds, "kmalloc", "Test the kmalloc implementation.",
		&kmalloc_command);

    add_command(cmds, "page", "Test the pagination feature.", &page_command);
}

void user_input(char *input) {
  if (!check_command(cmds, input))
    {
      kprint("You said: ");
      kprint(input);
      kprint("\n> ");

    }
  else
    kprint("\n> ");
}

static void
fault_test()
{
  uint32_t *ptr = (uint32_t*)0xA0000000;
  uint32_t do_page_fault = *ptr;
  UNUSED(do_page_fault);
}

static void
heap_command()
{
  if (kheap)
    kprint("(kheap is ON)");
  else
    {
      kprint("(kheap is OFF)");
      kheap = create_heap(KHEAP_START, KHEAP_START+KHEAP_INITIAL_SIZE,
			  0xCFFFF000, 0, 0);

    }
  kprint("\n");
  return;
  uint32_t* b = halloc(8, 0,kheap);
  UNUSED(b);
  /*
    uint32_t c = halloc(8, 0,kheap);
    kprint("a: ");
    kprint_hex(b);
    kprint(", b: ");
    kprint_hex(b);
    kprint("\nc: ");
    kprint_hex(c);
  */
  /*
    hfree(c);
    hfree(b);
  */
  /*
    uint32_t d = kmalloc_sz(12);
    kprint(", d: ");
    kprint_hex(d);
  */
}

static void
kmalloc_command()
{
  kprint("Kmalloc addr ");
  
  if (kheap)
    kprint("(kheap is ON)");
  else
    kprint("(kheap is OFF)");

  kprint(" : ");
  
  void* i = kmalloc_sz(8);
  ASSERT(i);
  kprint_hex((uint32_t)i);
  kprint("\n");

}

/** Halt the CPU
  *
  */
void
halt()
{
  kprint("Stopping the CPU. Bye!\n");
  asm volatile("hlt");
}

static void page_command()
{
  uint32_t phys_addr;
  void* page = kmalloc(1000, 1, &phys_addr);
  char page_str[16] = "";
  hex_to_ascii((int)page, page_str);
  char phys_str[16] = "";
  hex_to_ascii(phys_addr, phys_str);
  kprint("Page: ");
  kprint(page_str);
  kprint(", physical address: ");
  kprint(phys_str);
  kprint("\n");

  //
  uint32_t p = get_page(0x0C, true, KERNEL_DIRECTORY);
  UNUSED(p);
  
}
