#include "panic.h"

#include "../drivers/screen.h"
#include "kernel.h"

#ifdef _TESTS
#  include <stdio.h>
#endif

extern void panic(const char *message, const char *file, uint32_t line)
{
    // We encountered a massive problem and have to stop.
    asm volatile("cli"); // Disable interrupts.

    kprint("PANIC(");
    kprint((char*)message);
    kprint(") at ");
    kprint((char*)file);
    kprint(":");
    kprint_dec(line);
    kprint("\n");
    halt(); // Halt the CPU
}

extern void panic_assert(const char *file, uint32_t line, const char *desc)
{
#ifndef _TESTS  
    // An assertion failed, and we have to panic.
    asm volatile("cli"); // Disable interrupts.
    
    kprint("ASSERTION-FAILED(");
    kprint((char*)desc);
    kprint(") at ");
    kprint((char*)file);
    kprint(":");
    kprint_dec(line);
    kprint("\n");
    // Halt by going into an infinite loop.
    for(;;);
#else
    printf("ASSERTION-FAILED(%s) %s, %i", desc, file, line);
#endif
}
