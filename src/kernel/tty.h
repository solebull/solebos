/** \file tty.h
  * General console handling functions
  *
  */
#ifndef _TTY_H_
#define _TTY_H_

#include "../libc/bool.h"

typedef void (*command_ptr_t)(void);

typedef struct command_node{
  char* command;             //!< the command text lower case
  char* help;                //!< the help text
  command_ptr_t ptr;         //!< the pointer to result function
  struct command_node* next;
} command_t;

command_t* initialize_tty();
void add_command(command_t*, char*, const char*, command_ptr_t);
void tty_help(command_t*);
bool check_command(command_t*, char*);
#endif // !_TTY_H_
