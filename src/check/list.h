#ifndef __LIST_H__
#define __LIST_H__

/** The basic list type 
  *
  * A simple singly-linked list with void* data type.
  *
  */
typedef struct _List
{
  struct _List* next;   //!< The self-referenced next iden
  void*         data;   //!< The holded data
} List;

List* list_create(void *data);
List* list_prepend(List *old, void *data);
List* list_append(List *list, void *data);
void list_destroy(List *list);

#endif // !__LIST_H__
