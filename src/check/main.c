#include "check.h"

#include <stdio.h>

// Just a check library
START_TEST(test_1)
{
  assert(2 == 2);
}
END_TEST

START_TEST(test_2)
{
  assert(2 == 2);
}
END_TEST

START_TEST(test_3)
{
  assert(2 == 2);
}
END_TEST

START_TEST(test_4)
{
  assert(2 == 2);
}
END_TEST

START_TEST(test_5)
{
  assert(2 == 2);
}
END_TEST


START_TEST(test_fail)
{
  assert(2 == 3);
  int* ptr = NULL;
  ck_assert_ptr_ne(ptr, NULL);
}
END_TEST

int
main()
{

  Suite* s = suite_create("libc/oarray");
  SRunner* sr = srunner_create(s);

  /* Core test case */
  TCase* tc_core = tcase_create("Libc");

  tcase_add_test(tc_core, test_1);
  tcase_add_test(tc_core, test_2);
  tcase_add_test(tc_core, test_3);
  tcase_add_test(tc_core, test_4);
  tcase_add_test(tc_core, test_5);
  tcase_add_test(tc_core, test_fail);
  suite_add_tcase(s, tc_core);

  srunner_run_all(sr, CK_NORMAL);
  int number_failed = srunner_ntests_failed(sr);
  
  //srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
