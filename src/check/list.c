#include "list.h"

#include "check.h"   // USES NULL
#include <stdlib.h>  // USES malloc()

List*
list_create (void *data)
{
  List *list = malloc(sizeof(list));
  if (list)                        
   {
       list->data = data;          
       list->next = NULL;          
   }
  return list;               
 }

/** Prepend at the start of the list */
List*
list_prepend(List *old, void *data)
{
  List *list = list_create(data);
  if (list)
    list->next = old;
  return list;
}

List*
list_append(List *list, void *data)
{
    List **plist = &list;

    while (*plist)
       plist = &(*plist)->next;

    *plist = list_create(data);
    if (*plist)
       return list;
    else
       return NULL;
}

List *
list_remove_first(List *list)
{
    List *first = list;
    list = list->next;
    free(first);
    return list;
}

void
list_destroy(List *list)
{
  while (list)
    list = list_remove_first(list);
}
