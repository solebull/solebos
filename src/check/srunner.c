#include "srunner.h"

#include <stdlib.h>
#include <stdio.h>

// These ones are defined in check_impl.c
extern unsigned int nb_assert;
extern unsigned int nb_failed;

SRunner *
srunner_create(Suite * s)
{
  SRunner* r = malloc(sizeof(SRunner));
  r->suites        = list_create(s);
  r->ntests_failed = 0;
  return r;
}

void
srunner_add_suite(SRunner* r, Suite * s)
{
  list_append(r->suites, s);
}

unsigned int
srunner_ntests_failed(SRunner* sr)
{
  return sr->ntests_failed;
}

void
srunner_run_all(SRunner* s, ck_mode mode)
{
  UNUSED(mode);
  int nbtests = 0;
  
  List* lit = s->suites;
  while(lit)
    {
      Suite* ptr = (Suite*) lit->data;
      List* it = ptr->cases;
      while(it)
	{
	  TCase* ptr = (TCase*) it->data;
	  List* it2 = ptr->tests;
	  while(it2)
	    {
	      Test* ptr2 = (Test*) it2->data;
	      
	      test_run(ptr2);
	      //	  test_debug(ptr2);
	      it2 = it2->next;
	      ++nbtests;
	    }
	  it = it->next;
	}

      lit = lit->next;
    }

  printf("\n ");
  printf("Statistics :\n ");
  printf("  #tests : %d ", nbtests);
  printf("  #asserts : %d ", nb_assert);
  printf("  #failed : %d\n ", nb_failed);
}
