#include "tcase.h"

#include <stdlib.h> // USES malloc()
#include <string.h> // USES strdup()
#include <stdio.h>  // USES printf()

extern unsigned int nb_failed; //!< From check_impl.c

/** Create and return a new test case object
 * 
 * \param name The test case name.
 *
 * \return A newly created test case object pointer.
 *
 */
TCase*
tcase_create(const char* name)
{
  TCase* tc = malloc(sizeof(TCase));
  tc->name  = strdup(name);
  return tc;
}

/** Add a test to the given test case
  *
  * Will add a copy of `te` to the test case internal chained list.
  *
  * \param c  The case to add the test to.
  * \param te The te to be added.
  *
  */
void
tcase_add_test(TCase* c , const Test te)
{
  Test* t = test_create(te.name, te.file, te.line, te.funcptr);
  
  // If cases list not yet created
  if (c->tests)
    list_append(c->tests, (void*)t);
  else
    c->tests = list_create((void*)t);
}

/** Create and returns a new test
  *
  * \param name The test name.
  * \param file The source file name.
  * \param line The line the test occured at.
  * \param f    The test function.
  *
  */
Test*
test_create(const char* name, const char* file, unsigned int line, funcptr_t f)
{
  Test* t   = malloc(sizeof(Test));
  t->name    = strdup(name);
  t->file    = strdup(file);
  t->line    = line;
  t->funcptr = f;
  return t;
}

/** Output information about the given test
  *
  * \param t The test.
  *
  */
void
test_debug(const Test* t)
{
  if (!t)
    {
      printf("Can't debug NULL Test\n");
      return;
    }
  printf("Debugging TCase '%s' from '%s:%d' => ", t->name, t->file, t->line);
  printf("funcptr('%p')\n", t->funcptr);
}

/** Run the given test
  *
  * \param t The test.
  *
  */
void
test_run(Test* t)
{
  unsigned nbf = nb_failed;
  (t->funcptr)();
  if (nbf != nb_failed) 
    printf("\\-> '%s:%d' Test '%s' failed\n", t->file, t->line, t->name);
}
