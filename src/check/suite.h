#ifndef __SUITE_H__
#define __SUITE_H__

#include "tcase.h"
#include "list.h"

/** \file suite.h
  * A header used to export Suite public functions.
  *
  */


/** A test suite containing multiple test case 
  *
  * The suite must be instanciated using a call to suite_create() and
  * can be populated with \ref suite_add_tcase() calls.
  *
  */
typedef struct
{
  char* name;    //!< The suite name
  List* cases;   //!< A simple linked list of tests
}Suite;

Suite* suite_create(const char *name);
void   suite_add_tcase(Suite *s, TCase*);

#endif // !__SUITE_H__
