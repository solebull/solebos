
#include "suite.h"

#include "list.h"
#include "check.h" // USES NULL

#include <stdlib.h> // USES malloc()
#include <string.h> // USES strdup()
#include <stdio.h>  // USES printf()

/** Create a named test suite
  *
  * \param name The test suite name.
  *
  * \return A malloc'ed Suite pointer.
  *
  */
Suite*
suite_create(const char *name)
{
  Suite* s = malloc(sizeof(Suite));
  s->name = strdup(name);
  s->cases = NULL;
  return s;
}

/** Add a test case to the end of the given suite's case list
  *
  * \param s  A allocated Suite pointer.
  * \param tc The test case to be added.
  *
  */
void
suite_add_tcase (Suite *s, TCase* tc)
{
  // If cases list was already created
  if (s->cases)
    list_append(s->cases, tc);
  else
    s->cases = list_create(tc);
}
