#ifndef __SRUNNER_H__
#define __SRUNNER_H__

#include "list.h"
#include "suite.h"

// The only mode
#define CK_NORMAL 0 
#define UNUSED(x) (void)(x)

typedef int ck_mode; // NYI

/** The main runner
  *
  */
typedef struct
{
  List* suites;
  unsigned int ntests_failed;
} SRunner;

SRunner     *srunner_create(Suite * s);
void         srunner_add_suite(SRunner* r, Suite * s);
unsigned int srunner_ntests_failed(SRunner*);
void         srunner_run_all(SRunner*, ck_mode);

#endif // !__SRUNNER_H__

