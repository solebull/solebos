#include "check_impl.h"

#include <stdio.h>

unsigned int nb_assert = 0; //!< Total number of asserts
unsigned int nb_failed = 0; //!< Total number of failed ones

void
assert_impl(const char* val, bool b)
{
  nb_assert++;
  if (b)
    printf(".");
  else
    {
      printf("X\n");
      printf("| Failed assert '%s' (details below)\n", val);
      nb_failed++;
    }
}
