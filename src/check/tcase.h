#ifndef __TCASE_H__
#define __TCASE_H__

#include "list.h"

typedef void (*funcptr_t)();

/** A test case 
  * 
  * A case may hold multiple Test opbject in the tests linked list member.
  *
  */
typedef struct
{
  char* name;  //!< The name of this case
  List* tests; //!< The internal list of unit tests
}TCase;

/** A concrete test */
typedef struct 
{
  char* name;             //!< The test name
  // For a more debugging output
  char*        file;      //!< The file where the test occured
  unsigned int line;      //!< The line number
  funcptr_t    funcptr;   //!< The function pointer
}Test;

TCase* tcase_create(const char*);
void   tcase_add_test(TCase*, const Test tc);

Test* test_create(const char*, const char*, unsigned int, funcptr_t);
void  test_debug(const Test*);
void  test_run(Test* t);

#endif // !__TCASE_H__
