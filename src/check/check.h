#ifndef __CHECK__H__
#define __CHECK__H__

#include "tcase.h"
#include "../libc/bool.h"

/** \file check.h
  * A complete minimal libcheck replacement used to avoid segfault.
  *
  * May be ran inside qemu
  *
  */

// Is from stdlib.h
#define EXIT_SUCCESS 0 //!< A replacement for stdlib.h's success value
#define EXIT_FAILURE 1 //!< A replacement for stdlib.h's failure value

// Should define the libcheck thread model
#define CK_NORMAL 0  //!< The libcheck (and only implemented) thread model

#define STRINGIFY( x ) # x //!< Used to cast a preprocessor value to string

// Directly from https://github.com/libcheck/check/blob/master/src/check.h.in
/**
 * Start a unit test with START_TEST(unit_name), end with END_TEST.
 *
 * One must use braces within a START_/END_ pair to declare new variables
 *
 * @since 0.6.0
 */
#define START_TEST(__testname)                                          \
  static void __testname ## _fn();				        \
  static const Test __testname = { STRINGIFY( __testname ), __FILE__,	\
				    __LINE__, & __testname ## _fn } ;	\
  static void __testname ## _fn ()

#define END_TEST

// Certainly to handle __FILE__ and __LINE__
/*
static void __testname ## _fn ();\
static const TTest __testname ## _ttest = {""# __testname, __testname ## _fn, __FILE__, __LINE__};\
static const TTest * __testname = & __testname ## _ttest;\
static void __testname ## _fn ()
*/

/**
 *  End a unit test
 *
 * @since 0.6.0
 */
#define END_TEST

#define NULL ( (void *) 0) //!< A replacement for std lib's NULL value

/*
#ifndef BOOL_DEFINED
  typedef int bool;

#  define false 0
#  define true  1
#endif // !BOOL_DEFINED
*/

#include "suite.h"
#include "srunner.h"
#include "check_impl.h"

/** A simple assertion implementation
  *
  * Simply stringify the X parameter to easily print it out if necessary.
  *
  * \param X The call to be asserted.
  *
  */
#define assert(X) assert_impl(STRINGIFY( X ), X)

/** Check that two pointers are not equal
  *
  * \param PX First pointer to compare.
  * \param PY Second pointer to compare.
  *
  */
#define ck_assert_ptr_ne(PX, PY)					\
  assert_impl("ck_assert_ptr_ne(" STRINGIFY(PX) ", " STRINGIFY(PY) ")",  \
	      PX != PY)

/** Check that the given pointer is non NULL
  *
  * \param P The pointer to be tested.
  *
  */
#define ck_assert_ptr_nonnull(P) \
  assert_impl("ck_assert_ptr_nonnull(" STRINGIFY(P) ")", P != NULL)

/** Check that two integers are equal
  *
  * \param I First integer to compare.
  * \param J Second integer to compare.
  *
  */
#define ck_assert_int_eq(I, J) \
  assert_impl("ck_assert_int_eq(" STRINGIFY(I) ", " STRINGIFY(J) ")", I == J)
  
#endif // ! __CHECK__H__


