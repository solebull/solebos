#include "libc/oarray.h"
#include "kernel/panic.h" // Uses ASSERT

#include <stdio.h>
//#include <assert.h>

int
main()
{
  //initialise_paging();
  ordered_array_t* oa = create_ordered_array(10,
					    &standard_lessthan_predicate);
  /*  assert(oa && "Ordered Arry is NULL");
  assert(oa->array && "Ordered Arry is NULL");
  */
  printf("array is '0x%08x'\n", oa);
}
