# Solebos

[[_TOC_]]

We can see binary result with `xxd boot.bin`.

You may want to install all needed cross-compilation-related package with
`checkinstall`.

# Kernel cross-compiler

	export PREFIX="/usr/local/i386elfgcc"
	export TARGET=i386-elf
	export PATH="$PREFIX/bin:$PATH"

## binutils

Updated to 2.24 then 2.40 from the tutorial.

	cd ~/src
	curl -O http://ftp.gnu.org/gnu/binutils/binutils-2.40.tar.gz
	tar -xvf binutils-2.40.tar.gz

	mkdir binutils-build
	cd binutils-build
	../binutils-2.40/configure --target=$TARGET --enable-interwork \
		--enable-multilib --disable-nls --disable-werror --prefix=$PREFIX \
		2>&1 | tee configure.log
	make -j4		
	sudo make all install 2>&1 | tee make.log

## gcc

For an unknown reason, using `checkinstall` here will fail, at least
for the **install-gcc** rule.

	cd ~/src
	curl -O https://ftp.gnu.org/gnu/gcc/gcc-13.1.0/gcc-13.1.0.tar.gz
	tar -xvf gcc-13.1.0.tar.gz
	mkdir gcc-build
	cd gcc-13.1.0/
	./contrib/download_prerequisites
	cd ../gcc-build
	../gcc-13.1.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls \
		--disable-libssp --enable-languages=c --without-headers
	make all-gcc -j4
	make all-target-libgcc -j4
	sudo make install-gcc 
	sudo make install-target-libgcc 

The build step may be very long (several hours).

## gdb

Again the `checkinstall` faillure here. Use make install command if needed :

	cd ~/src
	curl -O http://ftp.rediris.es/mirror/GNU/gdb/gdb-9.2.tar.gz
	tar xf gdb-9.2.tar.gz
	mkdir gdb-build
	cd gdb-build
	export PREFIX="/usr/local/i386elfgcc"
	export TARGET=i386-elf
	../gdb-9.2/configure --target="$TARGET" --prefix="$PREFIX" --program-prefix=i386-elf-
	make -j4
	sudo make install

## .bashrc

Now we can add our new crosscompiler to the **$PATH** environment variable. 
Add the following line to the
`~/.bashrc` file :

	export PATH=/usr/local/i386elfgcc/bin/:$PATH

## Other dependencies

You should find the following package from you distribution provider :

	sudo apt install qemu-system-x86 nasm tigervnc-viewer

On manjaro, you can try :

	sudo pacman -S qemu-system-x86 nasm tigervnc

# Building

To build and run the project using qemu :

	cd src/
	make run

Then, connect to the opened local server with a VNC viewer :

	vncviewer 127.0.0.1:5900

Note : the port (here 5900) may change.


## Unit tests

I'm trying to unit test the whole project :

	cd src/tests/
	make check
	
If the latter command fails with a segfault, you can try

	make debug
	
## Documentation

From the project root :

	doxygen

Then, `open html/index.html` file with your favorite browser.
